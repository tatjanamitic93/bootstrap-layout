function formValidation() {
		var username = document.getElementById('username').value;
		var password = document.getElementById('password').value;
		return (validation(username, "Username") && validation(password, "Password"));
	}
	
	function validation(value, field) {
		if (value == null || value == "") {  
			alert(field + " can't be blank!");  
			return false;  
		} else if(value.length < 8) {  
			alert(field + " must be at least 8 characters long!");  
			return false;  
		} else {
			return true;
		}
	}

$(function () {
	$('#datepicker').datetimepicker({
		format: 'DD.MM.YYYY'
	});
});